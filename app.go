package main

import (

	"encoding/json"
	"log"
	"net/http"

	. "config"
	. "dao"
	. "models"

	"gopkg.in/mgo.v2/bson"

	"github.com/gorilla/mux"
	
	"github.com/ethereum/go-ethereum/crypto"
	"encoding/hex"
)

var config = Config{}
var customersDao = CustomersDAO{}



// POST register a customer
func CustomerRegisterEndPoint(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	
	var customer Customer
	if err := json.NewDecoder(r.Body).Decode(&customer); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

// Create an account
key, err := crypto.GenerateKey()
if(err != nil){
	log.Fatal(err)
} else{

// Get the address
address := crypto.PubkeyToAddress(key.PublicKey).Hex()
// 0x8ee3333cDE801ceE9471ADf23370c48b011f82a6

// Get the private key
privateKey := hex.EncodeToString(key.D.Bytes())
// 05b14254a1d0c77a49eae3bdf080f926a2df17d8e2ebdf7af941ea001481e57f

customer.Address = address
customer.PrivateKey = privateKey

} 
	customer.ID = bson.NewObjectId()
	if err := customersDao.Insert(customer); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJson(w, http.StatusCreated, customer)
}

/*
// POST login
func CustomerLoginEndPoint(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var customer Customer
	if err := json.NewDecoder(r.Body).Decode(&customer); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	
	respondWithJson(w, http.StatusCreated, customer)
}
*/

// GET list of customers
func AllCustomersEndPoint(w http.ResponseWriter, r *http.Request) {
	customers, err := customersDao.FindAll()
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJson(w, http.StatusOK, customers)
}

// GET a customer by its ID
func FindCustomerEndpoint(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	customer, err := customersDao.FindById(params["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Customer ID")
		return
	}
	respondWithJson(w, http.StatusOK, customer)
}


// PUT update an existing customer
func UpdateCustomerEndPoint(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var customer Customer
	if err := json.NewDecoder(r.Body).Decode(&customer); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	if err := customersDao.Update(customer); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJson(w, http.StatusOK, map[string]string{"result": "success"})
}

// DELETE an existing customer
func DeleteCustomerEndPoint(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var customer Customer
	if err := json.NewDecoder(r.Body).Decode(&customer); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	if err := customersDao.Delete(customer); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJson(w, http.StatusOK, map[string]string{"result": "success"})
}


func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJson(w, code, map[string]string{"error": msg})
}

func respondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

// Parse the configuration file 'config.toml', and establish a connection to DB
func init() {
	config.Read()

	customersDao.Server = config.Server
	customersDao.Database = config.Database
	customersDao.Connect()
}

// Define HTTP request routes
func main() {
	r := mux.NewRouter()

	// Customer services
	r.HandleFunc("/customer/registration", CustomerRegisterEndPoint).Methods("POST")
	// r.HandleFunc("/customer/login", CustomerLoginEndPoint).Methods("POST")
	r.HandleFunc("/customers", AllCustomersEndPoint).Methods("GET")
	r.HandleFunc("/customers", UpdateCustomerEndPoint).Methods("PUT")
	r.HandleFunc("/customers", DeleteCustomerEndPoint).Methods("DELETE")
	r.HandleFunc("/customers/{id}", FindCustomerEndpoint).Methods("GET")

	if err := http.ListenAndServe(":3000", r); err != nil {
		log.Fatal(err)
	}
}
