package models

import "gopkg.in/mgo.v2/bson"

type Customer struct {
	ID          bson.ObjectId `bson:"_id" json:"id"`
	Name        string        `bson:"name" json:"name"`
	Email  string        `bson:"email" json:"email"`
	Password string        `bson:"password" json:"password"`
	EncPass string        `bson:"encPass" json:"encPass"`
	Address string  `bson:"address" json:"address"`
	PrivateKey string `bson:"privateKey" json:"privateKey"`
}
